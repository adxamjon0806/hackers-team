import Footer from "./components/Footer";
import Nav from "./components/Nav";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/homePage";
import Fees from "./pages/fees";
import Contact from "./pages/contact";
import WhyUs from "./pages/whyUs";
import HowWork from "./pages/howItWorks";
import Prepaid from "./pages/prepaidCard";
import About from "./pages/about";

function App() {
  return (
    <div className="App">
      <Nav />
      <main>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/why" element={<WhyUs />} />
          <Route path="/how" element={<HowWork />} />
          <Route path="/prepaid" element={<Prepaid />} />
          <Route path="/fees" element={<Fees />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
      </main>
      <Footer />
    </div>
  );
}

export default App;
